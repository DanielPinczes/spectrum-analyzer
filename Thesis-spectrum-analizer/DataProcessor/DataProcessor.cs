﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading;

namespace Thesis_spectrum_analyzer
{
    class DataProcessor
    {
        bool isStarted = false;

        private SpectrumSamples model;
        private SerialPortHandler serialHandler = SerialPortHandler.GetInstance();

        private Thread process;

        public SpectrumSamples Model { get => model; set => model = value; }

		public const int RAW_DATA_SIZE = 192;
        public const int NUMBER_OF_SAMPLES_TO_DISPLAY = 128;

		public void Start()
		{
			isStarted = true;
			SubscribeToDataReceived();

		}
		public void Stop()
		{
			if (isStarted == true)
			{
				process.Abort();
			}
			isStarted = false;
		}

		private void SubscribeToDataReceived()
		{
			serialHandler.OnDataReceived += Process;
		}

		private void Process(byte[] rawData)
		{
			double[] samples = TransformToSamples(rawData);
            double[] spectrum = ComplexToScalar(FFT(SamplesToComplex(samples)));
			UpdateModel(spectrum);
		}

        // Signal processing functions...
        #region
        double[] TransformToSamples(byte[] rawData)
        {
            double[] samples = new double[128];

            for (int i = 0, j = 0; i < RAW_DATA_SIZE; i += 3, j += 2)
            {
                double first = rawData[i] << 4 | (rawData[i + 1] & 0xF0) >> 4;
                double second = (rawData[i + 1] & 0x0F) << 8 | rawData[i + 2];

                samples[j] = first;
                samples[j + 1] = second;
            }

            return samples;
        }

        private Complex[] SamplesToComplex(double[] samples)
        {
            double w = 2 * Math.PI;
            double f = NUMBER_OF_SAMPLES_TO_DISPLAY;
            Complex[] complexSamples = new Complex[NUMBER_OF_SAMPLES_TO_DISPLAY];
            double[] magnitude = new double[NUMBER_OF_SAMPLES_TO_DISPLAY];

            for (int i = 0; i < NUMBER_OF_SAMPLES_TO_DISPLAY; i++)
            {
                complexSamples[i] = Complex.FromPolarCoordinates(samples[i], f * w / 180);
            }

            return complexSamples;
        }

        private double[] DFT(Complex[] samples)
        {
            int N = samples.Length;
            Complex[] X = new Complex[N];
            double[] spectrum = new double[N];

            for (int k = 0; k < N; k++)
            {
                X[k] = new Complex();

                for (int n = 0; n < N; n++)
                {
                    Complex tmp = Complex.FromPolarCoordinates(1, -2 * Math.PI * n * k / N);
                    tmp *= samples[n];
                    X[k] += tmp;
                }
                if (k < N - 1) spectrum[k] = X[k].Magnitude;
            }
            return spectrum;
        }

        private Complex[] FFT(Complex[] samples)
        {
            int N = samples.Length;
            Complex[] X = new Complex[N];
            Complex[] e, E, d, D;

            if (N == 1)
            {
                X[0] = samples[0];
                return X;
            }

            int k = 0;
            e = new Complex[N / 2];
            d = new Complex[N / 2];

            for (k = 0; k < N / 2; k++)
            {
                e[k] = samples[k * 2];
                d[k] = samples[k * 2 + 1];

            }

            E = FFT(e);
            D = FFT(d);

            for (k = 0; k < N / 2; k++)
            {
                Complex temp = Complex.FromPolarCoordinates(1, -2 * Math.PI * k / N);
                D[k] *= temp;
            }

            for (k = 0; k < N / 2; k++)
            {
                X[k] = E[k] + D[k];
                X[k + N / 2] = E[k] - D[k];
            }

            return X;
        }

        double[] ComplexToScalar(Complex[] samples)
        {
            int N = samples.Length;
            double[] X = new double[N];

            for (int i = 0; i < N; i++)
            {
                X[i] = samples[i].Magnitude;
            }

            return X;
        }
        #endregion

        void UpdateModel(double[] spectrum)
        {
            model.Values = spectrum;
        }

		// For testing...
		#region
		private int offset;
		private double[] SinusGenerator()
		{
			double[] sinusValues = new double[128];

			for (int i = 0; i < 128; i++)
			{
				sinusValues[i] = 10 * System.Math.Sin(((Math.PI * 2) / 128) * (i - offset));
			}

			offset = ++offset % 128;
			return sinusValues;
		}
		#endregion
	}
}
