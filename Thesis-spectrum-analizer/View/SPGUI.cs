﻿using System;
using System.Windows.Forms;

namespace Thesis_spectrum_analyzer
{
    public partial class SpectrumGUI : Form
    {

		public event EventHandler OnUIInteraction;

		private void SendEvent(object sender, EventArgs args)
		{
			OnUIInteraction?.Invoke(sender, args);
		}

        public SpectrumGUI()
        {
            InitializeComponent();
        }

		// Serial events
		#region

		#endregion


		// MCU control events
		#region
		private void startADC_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}

		private void stopADC_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}

		private void resetMCU_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}
		#endregion

		// Ploting options events
		#region
		private void startPloting_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}

		#endregion

		private void pausePloting_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}

		private void stopPloting_Click(object sender, EventArgs e)
		{
			SendEvent(sender, e);
		}

        private void openSerialPort_Click(object sender, EventArgs e)
        {
            SendEvent(sender, e);
        }

        private void closeSerialPort_Click(object sender, EventArgs e)
        {
            SendEvent(sender, e);
        }
    }
}
