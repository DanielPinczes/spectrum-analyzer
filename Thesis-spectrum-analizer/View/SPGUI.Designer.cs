﻿using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Thesis_spectrum_analyzer
{
    partial class SpectrumGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.commandConsole = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.resetMCU = new System.Windows.Forms.Button();
            this.comPort = new System.Windows.Forms.ComboBox();
            this.startPloting = new System.Windows.Forms.Button();
            this.stopADC = new System.Windows.Forms.Button();
            this.openSerialPort = new System.Windows.Forms.Button();
            this.startADC = new System.Windows.Forms.Button();
            this.baudRate = new System.Windows.Forms.TextBox();
            this.dataBit = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.stopBit = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.parity = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.closeSerialPort = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pausePloting = new System.Windows.Forms.Button();
            this.stopPloting = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.spectrumChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumChart)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.commandConsole, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.spectrumChart, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1232, 594);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // commandConsole
            // 
            this.commandConsole.FormattingEnabled = true;
            this.commandConsole.Location = new System.Drawing.Point(497, 392);
            this.commandConsole.Name = "commandConsole";
            this.commandConsole.Size = new System.Drawing.Size(732, 199);
            this.commandConsole.TabIndex = 5;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 6;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel6, 2);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.Controls.Add(this.resetMCU, 3, 6);
            this.tableLayoutPanel6.Controls.Add(this.comPort, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.startPloting, 5, 2);
            this.tableLayoutPanel6.Controls.Add(this.stopADC, 3, 4);
            this.tableLayoutPanel6.Controls.Add(this.openSerialPort, 0, 7);
            this.tableLayoutPanel6.Controls.Add(this.startADC, 3, 2);
            this.tableLayoutPanel6.Controls.Add(this.baudRate, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.dataBit, 1, 4);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 6);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.stopBit, 1, 5);
            this.tableLayoutPanel6.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.parity, 1, 6);
            this.tableLayoutPanel6.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.closeSerialPort, 1, 7);
            this.tableLayoutPanel6.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.pausePloting, 5, 3);
            this.tableLayoutPanel6.Controls.Add(this.stopPloting, 5, 4);
            this.tableLayoutPanel6.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label9, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.label10, 3, 1);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 392);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 8;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(488, 199);
            this.tableLayoutPanel6.TabIndex = 6;
            // 
            // resetMCU
            // 
            this.resetMCU.Location = new System.Drawing.Point(225, 143);
            this.resetMCU.Name = "resetMCU";
            this.resetMCU.Size = new System.Drawing.Size(100, 23);
            this.resetMCU.TabIndex = 15;
            this.resetMCU.Text = "Reset MCU";
            this.resetMCU.UseVisualStyleBackColor = true;
            this.resetMCU.Click += new System.EventHandler(this.resetMCU_Click);
            // 
            // comPort
            // 
            this.comPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPort.FormattingEnabled = true;
            this.comPort.Location = new System.Drawing.Point(109, 29);
            this.comPort.Name = "comPort";
            this.comPort.Size = new System.Drawing.Size(100, 21);
            this.comPort.TabIndex = 6;
            // 
            // startPloting
            // 
            this.startPloting.Location = new System.Drawing.Point(371, 29);
            this.startPloting.Name = "startPloting";
            this.startPloting.Size = new System.Drawing.Size(100, 23);
            this.startPloting.TabIndex = 16;
            this.startPloting.Text = "Start";
            this.startPloting.UseVisualStyleBackColor = true;
            this.startPloting.Click += new System.EventHandler(this.startPloting_Click);
            // 
            // stopADC
            // 
            this.stopADC.Location = new System.Drawing.Point(225, 87);
            this.stopADC.Name = "stopADC";
            this.stopADC.Size = new System.Drawing.Size(100, 23);
            this.stopADC.TabIndex = 14;
            this.stopADC.Text = "Stop ADC";
            this.stopADC.UseVisualStyleBackColor = true;
            this.stopADC.Click += new System.EventHandler(this.stopADC_Click);
            // 
            // openSerialPort
            // 
            this.openSerialPort.Location = new System.Drawing.Point(3, 172);
            this.openSerialPort.Name = "openSerialPort";
            this.openSerialPort.Size = new System.Drawing.Size(100, 23);
            this.openSerialPort.TabIndex = 5;
            this.openSerialPort.Text = "Open";
            this.openSerialPort.UseVisualStyleBackColor = true;
            this.openSerialPort.Click += new System.EventHandler(this.openSerialPort_Click);
            // 
            // startADC
            // 
            this.startADC.Location = new System.Drawing.Point(225, 29);
            this.startADC.Name = "startADC";
            this.startADC.Size = new System.Drawing.Size(100, 23);
            this.startADC.TabIndex = 13;
            this.startADC.Text = "Start ADC";
            this.startADC.UseVisualStyleBackColor = true;
            this.startADC.Click += new System.EventHandler(this.startADC_Click);
            // 
            // baudRate
            // 
            this.baudRate.Enabled = false;
            this.baudRate.Location = new System.Drawing.Point(109, 58);
            this.baudRate.Name = "baudRate";
            this.baudRate.Size = new System.Drawing.Size(100, 20);
            this.baudRate.TabIndex = 9;
            // 
            // dataBit
            // 
            this.dataBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataBit.FormattingEnabled = true;
            this.dataBit.Location = new System.Drawing.Point(109, 87);
            this.dataBit.Name = "dataBit";
            this.dataBit.Size = new System.Drawing.Size(100, 21);
            this.dataBit.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Parity";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Stop bit";
            // 
            // stopBit
            // 
            this.stopBit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stopBit.FormattingEnabled = true;
            this.stopBit.Location = new System.Drawing.Point(109, 116);
            this.stopBit.Name = "stopBit";
            this.stopBit.Size = new System.Drawing.Size(100, 21);
            this.stopBit.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Data bit";
            // 
            // parity
            // 
            this.parity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.parity.FormattingEnabled = true;
            this.parity.Location = new System.Drawing.Point(109, 143);
            this.parity.Name = "parity";
            this.parity.Size = new System.Drawing.Size(100, 21);
            this.parity.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Baud rate";
            // 
            // closeSerialPort
            // 
            this.closeSerialPort.Location = new System.Drawing.Point(109, 172);
            this.closeSerialPort.Name = "closeSerialPort";
            this.closeSerialPort.Size = new System.Drawing.Size(100, 23);
            this.closeSerialPort.TabIndex = 12;
            this.closeSerialPort.Text = "Close";
            this.closeSerialPort.UseVisualStyleBackColor = true;
            this.closeSerialPort.Click += new System.EventHandler(this.closeSerialPort_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Serial Line";
            // 
            // pausePloting
            // 
            this.pausePloting.Location = new System.Drawing.Point(371, 58);
            this.pausePloting.Name = "pausePloting";
            this.pausePloting.Size = new System.Drawing.Size(100, 23);
            this.pausePloting.TabIndex = 17;
            this.pausePloting.Text = "Pause";
            this.pausePloting.UseVisualStyleBackColor = true;
            this.pausePloting.Click += new System.EventHandler(this.pausePloting_Click);
            // 
            // stopPloting
            // 
            this.stopPloting.Location = new System.Drawing.Point(371, 87);
            this.stopPloting.Name = "stopPloting";
            this.stopPloting.Size = new System.Drawing.Size(100, 23);
            this.stopPloting.TabIndex = 18;
            this.stopPloting.Text = "Stop";
            this.stopPloting.UseVisualStyleBackColor = true;
            this.stopPloting.Click += new System.EventHandler(this.stopPloting_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label7, 2);
            this.label7.Location = new System.Drawing.Point(3, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Serial";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.tableLayoutPanel6.SetColumnSpan(this.label8, 4);
            this.label8.Location = new System.Drawing.Point(119, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Hardware options";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(385, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Software options";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(225, 13);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 25;
            this.label10.Text = "MCU control";
            // 
            // spectrumChart
            // 
            chartArea2.Name = "ChartArea1";
            this.spectrumChart.ChartAreas.Add(chartArea2);
            this.tableLayoutPanel1.SetColumnSpan(this.spectrumChart, 3);
            legend2.Name = "Legend1";
            this.spectrumChart.Legends.Add(legend2);
            this.spectrumChart.Location = new System.Drawing.Point(3, 3);
            this.spectrumChart.Name = "spectrumChart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.spectrumChart.Series.Add(series2);
            this.spectrumChart.Size = new System.Drawing.Size(1226, 383);
            this.spectrumChart.TabIndex = 7;
            this.spectrumChart.Text = "chart1";
            // 
            // SpectrumGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1256, 618);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SpectrumGUI";
            this.Text = "Spectrum analyzer - Thesis v2.0 ";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button openSerialPort;
		private System.Windows.Forms.ComboBox comPort;
        private System.Windows.Forms.ComboBox dataBit;
        private System.Windows.Forms.TextBox baudRate;
        private System.Windows.Forms.ComboBox stopBit;
        private System.Windows.Forms.ComboBox parity;
        private System.Windows.Forms.Button closeSerialPort;
        private System.Windows.Forms.Button resetMCU;
        private System.Windows.Forms.Button stopADC;
        private System.Windows.Forms.Button startADC;
        private System.Windows.Forms.Button stopPloting;
        private System.Windows.Forms.Button pausePloting;
        private System.Windows.Forms.Button startPloting;
        private System.Windows.Forms.ListBox commandConsole;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataVisualization.Charting.Chart spectrumChart;

        public Chart SpectrumChart { get => spectrumChart; set => spectrumChart = value; }
		public ComboBox ComPort { get => comPort; set => comPort = value; }
		public ComboBox DataBit { get => dataBit; set => dataBit = value; }
		public TextBox BaudRate { get => baudRate; set => baudRate = value; }
		public ComboBox StopBit { get => stopBit; set => stopBit = value; }
		public ComboBox Parity { get => parity; set => parity = value; }
	}
}

