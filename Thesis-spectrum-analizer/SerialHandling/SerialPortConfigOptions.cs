﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis_spectrum_analyzer
{
	struct SerialPortConfigOptions
	{
		public List<string> PortNames;
		public List<int> BaudRates;
		public List<int> DataBits;
		public List<int> Stopbits;
		public List<string> Parities;
	}
}
