﻿using System;

namespace Thesis_spectrum_analyzer
{
	class SerialPortConfig
	{
		private String portName;
        private int baudRate;
        private int dataBits;
        private int stopBits;
        private String parity;

        public string PortName { get => portName; set => portName = value; }
        public int BaudRate { get => baudRate; set => baudRate = value; }
        public int DataBits { get => dataBits; set => dataBits = value; }
        public int StopBits { get => stopBits; set => stopBits = value; }
        public string Parity { get => parity; set => parity = value; }
    }
}
