﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace Thesis_spectrum_analyzer
{
	class SerialPortHandler
	{
		private SerialPortHandler() { }

		private static SerialPortHandler instance = new SerialPortHandler();

		public static SerialPortHandler GetInstance()
		{
			return instance;
		}

		private SerialPortConfig config;
		private SerialPort serialPort;
		internal SerialPortConfig Config { get => config; set => config = value; }

		private const int READ_TIMEOUT = 500;
		private const int WRITE_TIMEOUT = 500;

		public event DataReceivedHandler OnDataReceived;
		public delegate void DataReceivedHandler(byte[] sample);

		public void Open()
		{
			Configure();
			serialPort.Open();
			StartRead();
		}

		public void Close()
		{
			Console.WriteLine("CLosing serial port...");
			if (serialPort != null)
			{
				serialPort.Close();
			}
		}

		public void Write(MCUCommand command)
		{
			// TODO: if reading from port is in progress, it must be suspended.
			serialPort.Write(new byte[] { (byte)command }, 0, 0);
		}

		private void StartRead()
		{
			serialPort.DataReceived += delegate (object sender, SerialDataReceivedEventArgs args) { Read(); };
		}

		private void Read()
		{
            if (serialPort.BytesToRead >= 192)
            {
                byte[] buffer = new byte[DataProcessor.RAW_DATA_SIZE];
                serialPort.Read(buffer, 0, DataProcessor.RAW_DATA_SIZE);

                if (OnDataReceived != null) OnDataReceived(buffer);
            }
		}

		private void Configure()
		{
			if (config == null)
			{
				throw new SystemException("Serial port cannot be open without configuration.");
			}

			serialPort = new SerialPort();

			serialPort.PortName = config.PortName;
			serialPort.BaudRate = config.BaudRate;
			serialPort.DataBits = config.DataBits;
			serialPort.StopBits = transformStopBits(config.StopBits);
			serialPort.Parity = transformParity(config.Parity);
            serialPort.ReceivedBytesThreshold = 192;
            serialPort.ReadBufferSize = 192;

			serialPort.ReadTimeout = READ_TIMEOUT;
			serialPort.WriteTimeout = WRITE_TIMEOUT;
		}

		private static StopBits transformStopBits(int stopBits)
		{
			switch (stopBits)
			{
				case 0: return StopBits.None;
				case 1: return StopBits.One;
				case 2: return StopBits.Two;
				default: throw new SystemException("Invalid configuration.");
			}
		}

		private static Parity transformParity(string parity)
		{
			switch (parity)
			{
				case "NONE": return Parity.None;
				case "EVEN": return Parity.Even;
				case "ODD": return Parity.Odd;
				default: throw new SystemException("Invalid configuration.");
			}
		}

		// For testing...
		#region
		private Timer mockDataReceivedTimer;

		private void StartMocking()
		{
			mockDataReceivedTimer = new Timer(delegate(object obj) { MockDataReceivedEvent(); }, null, 0, 1 * 1000);
		}

		private void MockDataReceivedEvent()
		{
			if (OnDataReceived != null) OnDataReceived(new byte[DataProcessor.RAW_DATA_SIZE]);
		}
		#endregion
	}
}
