﻿using System.Collections.Generic;
using System.IO.Ports;

namespace Thesis_spectrum_analyzer
{
	class SerialPortConfigLoader
	{
		private static SerialPortConfigOptions defaultOptions = new SerialPortConfigOptions
		{
			
			PortNames = new List<string>(SerialPort.GetPortNames()),
			BaudRates = new List<int>() { 3686400 },
			DataBits = new List<int>() { 8, 9 },
			Stopbits = new List<int>() { 1, 2 },
			Parities = new List<string>() { "EVEN", "ODD", "NONE" }
		};

		public static SerialPortConfigOptions LoadConfig()
		{
			return defaultOptions;
		}
	}
}
