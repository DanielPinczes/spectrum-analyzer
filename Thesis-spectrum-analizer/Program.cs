﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Thesis_spectrum_analyzer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Creating MVC objects...
            SpectrumGUI view = new SpectrumGUI();
            SpectrumSamples model = new SpectrumSamples();
            SpectrumGUIController controller = new SpectrumGUIController();

            // Configuring controller to have acces to view and model...
            controller.View = view;
            controller.Model = model;

			// Subscribe controller to be notified form related events are published...
			controller.SubscribeEvents();

            DataProcessor dataProcessor = new DataProcessor();
            dataProcessor.Model = model;

            dataProcessor.Start();

			view.FormClosing += (sender, args) => dataProcessor.Stop();

            Application.Run(view);
        }
    }
}
