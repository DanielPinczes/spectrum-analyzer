﻿namespace Thesis_spectrum_analyzer
{
	public enum MCUCommand
	{
		START_ADC = 255,
		STOP_ADC = 254,
		RESET_MCU = 253
	}
}

