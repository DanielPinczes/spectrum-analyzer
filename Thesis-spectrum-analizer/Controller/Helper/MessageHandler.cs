﻿using System;

namespace Thesis_spectrum_analyzer
{
	class MessageHandler
	{
		private SerialPortHandler serialPortHandler = SerialPortHandler.GetInstance();

		public void SendMessage(MCUCommand command)
		{
            serialPortHandler.Write(command);
		}
	}
}
