﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Thesis_spectrum_analyzer
{
	class SpectrumGUIController
	{
		private SpectrumGUI view;
		private SpectrumSamples model;

		private MessageHandler messageHandler = new MessageHandler();

        private SerialPortHandler serialPortHandler = SerialPortHandler.GetInstance();

		private System.Threading.Timer chartUpdateTimer;

		private const int plotingDelay = 0;
		private const int plotingPeriod = 100;

		public SpectrumGUI View { get => view; set => view = value; }
		public SpectrumSamples Model { get => model; set => model = value; }

		public void SubscribeEvents()
		{
			if (this.view == null)
			{
				throw new SystemException("'view' is not set for controller. Unable to subscribe to events");
			}

			view.Shown += (sender, args) => onViewStarted();
			view.FormClosing += (sender, args) => onViewClosing();

			view.OnUIInteraction += HandleOnUIInteraction;
		}

		private void onViewStarted()
		{
			bootstrapSerialConfigurationOptions(SerialPortConfigLoader.LoadConfig());;
		}

		private void onViewClosing()
		{
            CloseSerialPort();
			StartPloting();
		}

		private void HandleOnUIInteraction(object sender, EventArgs args)
		{
			Button button = sender as Button;
			if (button != null)
			{
				switch (button.Name)
				{
                    case "openSerialPort": OpenSerialPort(); break;
                    case "closeSerialPort": CloseSerialPort(); break;

					case "startADC": messageHandler.SendMessage(MCUCommand.START_ADC); break;
					case "stopADC": messageHandler.SendMessage(MCUCommand.STOP_ADC); break;
					case "resetMCU": messageHandler.SendMessage(MCUCommand.RESET_MCU); break;

					case "startPloting": StartPloting(); break;
					case "stopPloting": StartPloting(); break;
					case "pausePloting": PausePloting(); break;
				}
			}
		}

		private void bootstrapSerialConfigurationOptions(SerialPortConfigOptions configOptions)
		{
			configOptions.PortNames.ForEach(comPort => view.ComPort.Items.Add(comPort));
			view.BaudRate.Text = configOptions.BaudRates[0].ToString();
			configOptions.DataBits.ForEach(numOfDataBit => view.DataBit.Items.Add(numOfDataBit));
			configOptions.Stopbits.ForEach(numOfStopbit => view.StopBit.Items.Add(numOfStopbit));
			configOptions.Parities.ForEach(parityType => view.Parity.Items.Add(parityType));

            view.ComPort.SelectedItem = "COM3";
            view.DataBit.SelectedItem = 8;
            view.StopBit.SelectedItem = 1;
            view.Parity.SelectedItem = "NONE";
		}

		private void UpdateChart()
		{
			double[] samples = Model.Values;

			Chart chart = View.SpectrumChart;

			if (chart.InvokeRequired)
			{
				chart.BeginInvoke((MethodInvoker)delegate () { chart.Series[0].Points.DataBindY(samples); });
			}
			else
			{
				chart.Series[0].Points.DataBindY(samples);
			}
		}

        private void OpenSerialPort()
        {
            SerialPortConfig serialPortConfig = new SerialPortConfig
            {
                PortName = view.ComPort.SelectedItem.ToString(),
                BaudRate = Int32.Parse(view.BaudRate.Text),
                DataBits = (int)view.DataBit.SelectedItem,
                StopBits = (int)view.StopBit.SelectedItem,
                Parity = view.Parity.SelectedItem.ToString()
            };

            serialPortHandler.Config = serialPortConfig;
            serialPortHandler.Open();
        }

        private void CloseSerialPort()
        {
			serialPortHandler.Close();
        }

		private void StartPloting()
		{
			if (this.chartUpdateTimer == null)
			{
				this.chartUpdateTimer = new System.Threading.Timer(delegate (object obj) { UpdateChart(); }, null, plotingDelay, plotingPeriod);
			}
			else
			{
				this.chartUpdateTimer.Change(plotingDelay, plotingPeriod);
			}
		}

		private void StopPloting()
		{
            /*
			if (this.chartUpdateTimer != null)
			{
				this.chartUpdateTimer.Dispose();
				this.view.SpectrumChart.Series[0].Points.DataBindY(new double[0] { });
				this.chartUpdateTimer = null;
			}
            */
		}

		private void PausePloting()
		{
			if (this.chartUpdateTimer != null)
			{
				this.chartUpdateTimer.Change(Timeout.Infinite, Timeout.Infinite);
			}
		}
	}
}
